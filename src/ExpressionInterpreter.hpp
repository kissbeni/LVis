
#ifndef _ExpressionInterpreter_hpp_
#define _ExpressionInterpreter_hpp_

// #define EXPRESSION_INTERPRETER_VERY_VERBOSE

#include "Engine/RendererBase.hpp"
#include <map>

namespace LVis {
    enum class ExpressionTokenType {
        UNKNOWN,
        SEPARATOR,
        LINEBREAK,
        EQUALS,
        START,
        VARIABLE,
        CONSTNUMBER,
        ADD,
        SUBSTRACT,
        MULTIPLY,
        DIVIDE,
        END
    };

    class ExpressionToken {
        public:
            ExpressionToken(const ExpressionTokenType type) :
                ExpressionToken(type, 0, "") { }
            
            const ExpressionTokenType GetType() const {
                return this->mType;
            }

        protected:
            ExpressionToken(const ExpressionTokenType type, const double value) :
                ExpressionToken(type, value, "") { }
            ExpressionToken(const ExpressionTokenType type, const std::string value) :
                ExpressionToken(type, 0, value) { }

            const std::string mValueString;
            const double mValueDouble;

        private:
            ExpressionToken(const ExpressionTokenType type, const double vald, const std::string vals) :
               mValueString(vals), mValueDouble(vald), mType(type) { }

            const ExpressionTokenType mType;
    };

    class ExpressionToken_Variable : public ExpressionToken {
        public:
            ExpressionToken_Variable(const std::string& varname) :
                ExpressionToken(ExpressionTokenType::VARIABLE, varname) { }

            const std::string GetName() const {
                return this->mValueString;
            }

            static bool IsValidVariableName(const std::string& cmd);
    };

    class ExpressionToken_ConstNumber : public ExpressionToken {
        public:
            ExpressionToken_ConstNumber(const double value) :
                ExpressionToken(ExpressionTokenType::CONSTNUMBER, value) { }

            const double GetValue() const {
                return this->mValueDouble;
            }
    };

    class ExpressionTokenizer
    {
        public:
            ExpressionTokenizer(const std::string& expr)
                : mExpr(expr), mPosition(0) { }

            void CreateTokens();
            void Print();

            const std::vector<ExpressionToken> GetSubExpression(int id, bool* success);
        private:
            bool ProcessNext();
            bool ProcessUnknown(const std::string& val);

            ExpressionTokenType TokenTypeByChar(const char token);

            const std::string mExpr;
            size_t mPosition;
            std::vector<ExpressionToken> mTokens;
    };

    class ExpressionInterpreter {

        public:
            ExpressionInterpreter(std::map<std::string, Vector<double>*>* refVariables, Engine::RendererBase* renderer) 
                : mVariables(refVariables), mRenderer(renderer) { }

            void InterpretExpression(const std::string& expr);

        private:
            bool ProcessTwoHandedOperation(const std::vector<ExpressionToken> leftHand, const std::vector<ExpressionToken> rightHand);

            Vector<double>* GetOrCreateVar(const std::string& name);

            std::map<std::string, Vector<double>*>* mVariables;
            Engine::RendererBase* mRenderer;
    };

}

#endif
