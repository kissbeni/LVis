
#include "ExpressionInterpreter.hpp"

namespace LVis {

static const char* allowed_variable_chars = "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
static const char* forbidden_variable_start_chars = "0123456789";

/// ### ExpressionToken_Variable ### ///

/// --- PUBLIC --- ///

bool ExpressionToken_Variable::IsValidVariableName(const std::string& cmd) {
    bool ok;

    if (cmd.empty()) {
        return false;
    }

    for (char c1 : cmd) {
        ok = false;

        for (int i = 0; allowed_variable_chars[i]; i++) {
            char c2 = allowed_variable_chars[i];

            if (c1 == c2) {
                ok = true;
                break;
            }
        }

        if (!ok) {
            return false;
        }
    }

    for (int i = 0; forbidden_variable_start_chars[i]; i++) {
        char c = forbidden_variable_start_chars[i];

        if (c == cmd[0]) {
            return false;
        }
    }
    
    return true;
}

/// ### ExpressionTokenizer ### ///

/// --- PUBLIC --- ///

void ExpressionTokenizer::CreateTokens() {
    if (mTokens.size() != 0) {
        throw "This tokenizer already has tokens!";
    }

    mTokens.push_back(ExpressionToken(ExpressionTokenType::START));

    while (ProcessNext()) ;

    mTokens.push_back(ExpressionToken(ExpressionTokenType::END));
}

void ExpressionTokenizer::Print() {
    printf("ExpressionTokenizer of '%s'\n", mExpr.c_str());
    printf("Tokens:\n");

    for (ExpressionToken t : mTokens) {
        if (t.GetType() == ExpressionTokenType::START) {
            printf("[START]\n");
        } else if (t.GetType() == ExpressionTokenType::END) {
            printf("[END]\n");
        } else if (t.GetType() == ExpressionTokenType::EQUALS) {
            printf("    [EQUALS]\n");
        } else if (t.GetType() == ExpressionTokenType::ADD) {
            printf("    [ADD]\n");
        } else if (t.GetType() == ExpressionTokenType::SUBSTRACT) {
            printf("    [SUBSTRACT]\n");
        } else if (t.GetType() == ExpressionTokenType::MULTIPLY) {
            printf("    [MULTIPLY]\n");
        } else if (t.GetType() == ExpressionTokenType::DIVIDE) {
            printf("    [DIVIDE]\n");
        } else if (t.GetType() == ExpressionTokenType::VARIABLE) {
            printf("    VAR(\"%s\")\n", ((ExpressionToken_Variable*)&t)->GetName().c_str());
        } else if (t.GetType() == ExpressionTokenType::CONSTNUMBER) {
            printf("    CONST(%.2f)\n", ((ExpressionToken_ConstNumber*)&t)->GetValue());
        }
    }
}

const std::vector<ExpressionToken> ExpressionTokenizer::GetSubExpression(int id, bool* success) {
    std::vector<ExpressionToken> res;

    *success = false;

    for (ExpressionToken t : mTokens) {
        if (id == -1) {
            *success = true;
            if (t.GetType() == ExpressionTokenType::END) {
                break;
            }

            res.push_back(t);
        } else {
            if (t.GetType() == ExpressionTokenType::START) {
                id--;
            }
        }
    }

    return res;
}

/// --- PRIVATE --- ///

bool ExpressionTokenizer::ProcessNext() {
    if (mExpr.length() <= mPosition) {
        return false;
    }

    char ch = mExpr[mPosition];

    #ifdef EXPRESSION_INTERPRETER_VERY_VERBOSE
    printf("ExpressionInterpreter::ProcessNext ch=%c, starting at %lu\n", ch, mPosition);
    #endif

    ExpressionTokenType type = TokenTypeByChar(ch);

    if (type == ExpressionTokenType::LINEBREAK ||
        type == ExpressionTokenType::SEPARATOR) {
        mPosition++;
        return true;
    }

    if (type == ExpressionTokenType::END) {
        mPosition++;
        mTokens.push_back(ExpressionToken(ExpressionTokenType::END));
        mTokens.push_back(ExpressionToken(ExpressionTokenType::START));
        return true;
    }

    if (type != ExpressionTokenType::UNKNOWN) {

        mPosition++;
        mTokens.push_back(ExpressionToken(type));
        return true;
    }

    std::string val = "";

    do {
        val += mExpr[mPosition];
        mPosition++;
    } while (mPosition < mExpr.length() && TokenTypeByChar(mExpr[mPosition]) == ExpressionTokenType::UNKNOWN);

    return ProcessUnknown(val);
}

bool ExpressionTokenizer::ProcessUnknown(const std::string& val) {
    if (val.empty()) {
        return false;
    }

    if (ExpressionToken_Variable::IsValidVariableName(val)) {
        
        #ifdef EXPRESSION_INTERPRETER_VERY_VERBOSE
        printf("ExpressionInterpreter::ProcessUnknown identified %s as VARIABLE\n", val.c_str());
        #endif

        mTokens.push_back(ExpressionToken_Variable(val));
        return true;
    }

    double x;
    char test;

    std::stringstream xStream(val);

    if ((!(xStream >> x)) || (xStream >> test)) {
        printf("ExpressionInterpreter::ProcessUnknown invalid token near %s\n", val.c_str());
        return false;
    }

    mTokens.push_back(ExpressionToken_ConstNumber(x));
    return true;
}

ExpressionTokenType ExpressionTokenizer::TokenTypeByChar(const char token) {
    switch (token) {
        case ' ':
        case '\t':
            return ExpressionTokenType::SEPARATOR;
        case '\n':
        case '\r':
            return ExpressionTokenType::LINEBREAK;
        case '=':
            return ExpressionTokenType::EQUALS;
        case '+':
            return ExpressionTokenType::ADD;
        case '-':
            return ExpressionTokenType::SUBSTRACT;
        case '*':
            return ExpressionTokenType::MULTIPLY;
        case '/':
            return ExpressionTokenType::DIVIDE;
        case ';':
            return ExpressionTokenType::END;
        default:
            return ExpressionTokenType::UNKNOWN;
    }
}

/// ### ExpressionInterpreter ### ///

/// --- PUBLIC --- ///

void ExpressionInterpreter::InterpretExpression(const std::string& expr) {
    ExpressionTokenizer* tokenizer = new ExpressionTokenizer(expr);

    tokenizer->CreateTokens();

    #ifdef EXPRESSION_INTERPRETER_VERY_VERBOSE
    tokenizer->Print();
    #endif

    bool success = true;
    for (int i = 0; success; i++) {
        const std::vector<ExpressionToken> tokens = tokenizer->GetSubExpression(i, &success);

        if (tokens.size() == 0) {
            continue;
        }

        #ifdef EXPRESSION_INTERPRETER_VERY_VERBOSE
        printf("expr %i has %lu tokens\n", i, tokens.size());
        #endif

        std::vector<ExpressionToken> leftHand;
        std::vector<ExpressionToken> rightHand;

        bool hand = false;
        for (ExpressionToken t : tokens) {
            if (t.GetType() == ExpressionTokenType::EQUALS) {
                if (hand) {
                    printf("WE ARE NOT READY FOR MULTIPLE EQUALS SIGNS!!!\n");
                    delete tokenizer;
                    return;
                }

                hand = true;
                continue;
            }

            if (hand) {
                rightHand.push_back(t);
            } else {
                leftHand.push_back(t);
            }
        }

        if (!ProcessTwoHandedOperation(leftHand, rightHand)) {
            break;
        }
    }

    delete tokenizer;
}


/// --- PRIVATE --- ///

bool ExpressionInterpreter::ProcessTwoHandedOperation(const std::vector<ExpressionToken> leftHand, const std::vector<ExpressionToken> rightHand) {
    if (leftHand.size() < 1) {
        printf("No tokens on the expression's left side!\n");
        return false;
    }

    if (rightHand.size() < 1) {
        printf("No tokens on the expression's right side!\n");
        return false;
    }

    if (leftHand.size() > 1) {
        printf("Multiple tokens on the expression's left side!\n");
        return false;
    }

    ExpressionToken leftSideToken = leftHand[0];

    if (leftSideToken.GetType() != ExpressionTokenType::VARIABLE) {
        printf("Token on the left side must be a VARIABLE token!\n");
        return false;
    }

    Vector<double>* leftSideVar = GetOrCreateVar(((ExpressionToken_Variable*)&leftSideToken)->GetName());

    printf("checking right hand tokens...\n");

    int numOperations = 0;
    bool lastWasVarOrConst = false;
    std::vector<double> mVarConsts;
    std::vector<Vector<double>*> mVarVectors;

    for (ExpressionToken t : rightHand) {
        if (t.GetType() == ExpressionTokenType::VARIABLE ||
            t.GetType() == ExpressionTokenType::CONSTNUMBER) {

            if (lastWasVarOrConst) {
                printf("Found data without operators!\n");
                return false;
            }

            lastWasVarOrConst = true;
        }

        if (t.GetType() == ExpressionTokenType::VARIABLE) {
            std::string name = ((ExpressionToken_Variable*)&t)->GetName();
            std::map<std::string, Vector<double>*>::iterator it;

            if ((it = mVariables->find(name)) == mVariables->end()) {
                if ((it = mVariables->find("$exprinternal$$" + name)) == mVariables->end()) {
                    printf("There is nothing named %s!\n", name.c_str());
                    return false;
                }
            }

            mVarConsts.push_back(NAN);
            mVarVectors.push_back(it->second);

            printf(" -> ok: %s (index: %lu)\n", name.c_str(), mVarVectors.size() - 1);
        } else if (t.GetType() == ExpressionTokenType::CONSTNUMBER) {
            mVarConsts.push_back(((ExpressionToken_ConstNumber*)&t)->GetValue());
            mVarVectors.push_back(nullptr);
        } else {
            if (!lastWasVarOrConst) {
                printf("Found multiple operators after eachother!\n");
                return false;
            }

            lastWasVarOrConst = false;

            numOperations++;
        }
    }

    if (numOperations == 0 && mVarVectors.size() == 1) {
        printf("looks like, we just have to copy a variable!\n");

        leftSideVar->SetXYZ(
            mVarVectors[0]->GetX(),
            mVarVectors[0]->GetY(),
            mVarVectors[0]->GetZ()
        );

        return true;
    }

    if (numOperations != 1) {
        printf("More than 1 operation is not yet supported!\n");
        return false;
    }

    switch (rightHand[1].GetType()) {
        case ExpressionTokenType::ADD:
            if (rightHand[0].GetType() != ExpressionTokenType::VARIABLE ||
                rightHand[2].GetType() != ExpressionTokenType::VARIABLE) {

                printf("Only vectors can be added to eachother!\n");
                return false;
            }

            *leftSideVar = *mVarVectors[0] + *mVarVectors[1];
            break;
        case ExpressionTokenType::SUBSTRACT:
            if (rightHand[0].GetType() != ExpressionTokenType::VARIABLE ||
                rightHand[2].GetType() != ExpressionTokenType::VARIABLE) {

                printf("Only vectors can be substracted from eachother!\n");
                return false;
            }

            *leftSideVar = *mVarVectors[0] + *mVarVectors[1];
            break;
        case ExpressionTokenType::MULTIPLY:
            if (rightHand[0].GetType() == ExpressionTokenType::VARIABLE &&
                rightHand[2].GetType() == ExpressionTokenType::CONSTNUMBER) {

                *leftSideVar = *mVarVectors[0] * mVarConsts[1];

            } else if ( rightHand[0].GetType() == ExpressionTokenType::CONSTNUMBER &&
                        rightHand[2].GetType() == ExpressionTokenType::VARIABLE) {

                *leftSideVar = mVarConsts[0] * *mVarVectors[1];
            } else {
                printf("Unsupported operation!\n");
                return false;
            }
            break;
        default:
            printf("Unhandled operation\n");
            return false;
    }

    return true;
}

Vector<double>* ExpressionInterpreter::GetOrCreateVar(const std::string& name) {
    bool addNew = true;
    Vector<double>* v = nullptr;

    for (std::pair<std::string, Vector<double>*> p : *mVariables) {
        if (p.first == name) {
            v = p.second;
            addNew = false;
            break;
        }
    }

    if (addNew) {
        v = new Vector<double>(0, 0, 0);
        mVariables->insert(std::pair<std::string, Vector<double>*>(name, v));
        mRenderer->AddVector(v);
    }

    return v;
}

}
