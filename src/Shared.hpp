
#ifndef _Shared_hpp_
#define _Shared_hpp_

#include <cmath>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#ifdef WINDOWS
#include <windows.h>
#include <atlbase.h>
#else
#include <unistd.h>
#endif

#ifndef PI
#define PI 3.14159265
#endif

namespace LVis {

struct Rectangle2D {
    int x, y, w, h;

    Rectangle2D() : Rectangle2D(0, 0, 0, 0) { }

    Rectangle2D(int x, int y, int width, int height) {
        this->x = x;
        this->y = y;
        this->w = width;
        this->h = height;
    }
};

struct ColorRGB {
    unsigned char r, g, b;

    ColorRGB(unsigned char r, unsigned char g, unsigned char b) {
        this->r = r;
        this->g = g;
        this->b = b;
    }
};

template<typename Out>
static inline void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

static inline std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

static inline double Rad2Deg(double x) {
    return x * 180.0 / PI;
}

static inline double Deg2Rad(double x) {
    return x * PI / 180.0;
}

static inline double AngleOfVectorsRad(double x1, double y1, double x2, double y2) {

    double  ab = (x1 * x2) + (y1 * y2);
    double  la = sqrt(pow(x1, 2) + pow(y1, 2));
    double  lb = sqrt(pow(x2, 2) + pow(y2, 2));

    return acos((ab / (la * lb)));
}

static inline double AngleOfLinesRad(
    double ax1, double ay1, double ax2, double ay2,
    double bx1, double by1, double bx2, double by2) {

    return AngleOfVectorsRad(ax2 - ax1, ay2 - ay1, bx2 - bx1, by2 - by1);
}

static inline double AngleOfVectorsDeg(double x1, double y1, double x2, double y2) {
    return Rad2Deg(AngleOfVectorsRad(x1, y1, x2, y2));
}

static inline double AngleOfLinesDeg(
    double ax1, double ay1, double ax2, double ay2,
    double bx1, double by1, double bx2, double by2) {

    return AngleOfVectorsDeg(ax2 - ax1, ay2 - ay1, bx2 - bx1, by2 - by1);
}

static inline bool ReadTextFileToString(const std::string& file, std::string& output) {
    std::ifstream f(file);

    if (!f.is_open()) {
        return false;
    }

    f.seekg(0, std::ios::end);

    size_t size = f.tellg();

    std::string buffer(size, ' ');
    f.seekg(0);
    f.read(&buffer[0], size);

    output.assign(buffer);
    return true;
}

static inline void mSleep(int milliseconds) {

    #ifdef WINDOWS
    Sleep(milliseconds);
    #else
    usleep(milliseconds * 1000);
    #endif
}

}

#endif
