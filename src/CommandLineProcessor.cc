
#include "CommandLineProcessor.hpp"
#include "ExpressionInterpreter.hpp"

#include <cstdio>
#include <iterator>

namespace LVis {

/// --- PUBLIC --- ///

void CommandLineProcessor::ProcessCommand(Engine::RendererBase* renderer, const std::string& cmdstr) {
    if (cmdstr.empty()) {
        return;
    }

    std::vector<std::string> tokens = split(cmdstr, ' ');

    printf("\n");

    std::vector<std::string> args;
    for (size_t i = 1; i < tokens.size(); i++) {
        args.push_back(tokens[i]);
    }

    std::map<std::string, Vector<double>*>::iterator it = mDefinedVectors.find("i");

    if (it == mDefinedVectors.end()) {
        mDefinedVectors.insert(std::pair<std::string, Vector<double>*>("i", new Vector<double>(1, 0, 0)));
    }

    it = mDefinedVectors.find("j");

    if (it == mDefinedVectors.end()) {
        mDefinedVectors.insert(std::pair<std::string, Vector<double>*>("j", new Vector<double>(0, 1, 0)));
    }

    it = mDefinedVectors.find("u");

    if (it == mDefinedVectors.end()) {
        mDefinedVectors.insert(std::pair<std::string, Vector<double>*>("u", new Vector<double>(0, 0, 1)));
    }

    for (std::pair<std::string, Vector<double>*> p : mDefinedVectors) {
        if (p.first == "i") {
            renderer->SetBaseVector('x', p.second);
        } else if (p.first == "j") {
            renderer->SetBaseVector('y', p.second);
        } else if (p.first == "u") {
            renderer->SetBaseVector('z', p.second);
        }
    }

    ProcessCommand(renderer, tokens[0], args);

    fflush(stdout);
}

/// --- PRIVATE --- ///

std::map<std::string, Vector<double>*> CommandLineProcessor::mDefinedVectors;

void CommandLineProcessor::ProcessCommand(Engine::RendererBase* renderer, const std::string& cmd, const std::vector<std::string>& args) {
    if (cmd == "help") {
        ShowHelp();
    } else if (cmd == "redraw") {
        renderer->RequestRedraw();
    } else if (cmd == "reset") {
        renderer->Reset();
        renderer->SetRenderScale(20.0);
    } else if (cmd == "delay") {
        if (args.size() < 1) {
            printf("Missing argument(s)!\n");
            return;
        }

        int x;
        char test;

        std::stringstream xStream(args[0]);

        if ((!(xStream >> x)) || (xStream >> test)) {
            printf("Invalid time value\n");
            return;
        }

        mSleep(x);
    } else if (cmd == "exit") {
        printf("Bye!\n");
        renderer->Destroy();
    } else if (cmd == "vector") {
        if (args.size() < 1) {
            printf("Missing argument(s)!\n");
            return;
        }

        bool addNew = true;
        Vector<double>* v = 0;

        std::string name = args[0];

        if (!ExpressionToken_Variable::IsValidVariableName(name)) {
            printf("Invalid character(s) in name: %s\n", name.c_str());
            return;
        }

        for (std::pair<std::string, Vector<double>*> p : mDefinedVectors) {
            if (p.first == name) {
                v = p.second;
                addNew = false;
                break;
            }
        }

        if (args.size() < 3) {
            if (v) {
                printf("x=%.2f y=%.2f z=%.2f visible=%c\n", v->GetX(), v->GetY(), v->GetZ(), v->IsVisible() ? 'y' : 'n');
            } else {
                printf("There is nothing named %s\n", name.c_str());
            }

            return;
        }

        if (addNew) {
            v = new Vector<double>(0, 0, 0);
        }

        double x, y, z = 0;
        char test;

        std::stringstream xStream(args[1]);

        if ((!(xStream >> x)) || (xStream >> test)) {
            printf("Invalid x coordinate\n");
            return;
        }

        std::stringstream yStream(args[2]);

        if ((!(yStream >> y)) || (yStream >> test)) {
            printf("Invalid y coordinate\n");
            return;
        }

        if (args.size() > 3) {
            std::stringstream zStream(args[3]);

            if ((!(zStream >> z)) || (zStream >> test)) {
                printf("Invalid z coordinate\n");
                return;
            }
        }

        v->SetXYZ(x, y, z);

        if (addNew) {
            renderer->AddVector(v, true);
        }

        mDefinedVectors.insert(std::pair<std::string, Vector<double>*>(name, v));
        renderer->RequestRedraw();
    } else if (cmd == "oshift") {
        if (args.size() < 2) {
            printf("Missing argument(s)!\n");
            return;
        }

        double x, y, z = 0;
        char test;

        std::stringstream xStream(args[0]);

        if ((!(xStream >> x)) || (xStream >> test)) {
            printf("Invalid x coordinate\n");
            return;
        }

        std::stringstream yStream(args[1]);

        if ((!(yStream >> y)) || (yStream >> test)) {
            printf("Invalid y coordinate\n");
            return;
        }

        if (args.size() > 2) {
            std::stringstream zStream(args[2]);

            if ((!(zStream >> z)) || (zStream >> test)) {
                printf("Invalid z coordinate\n");
                return;
            }
        }

        renderer->SetVirtualOrigoShift(x, y /*, z*/);
        renderer->RequestRedraw();
    } else if (cmd == "rotate") {
        if (args.size() < 2) {
            printf("Missing argument(s)!\n");
            return;
        }

        bool exists = false;
        Vector<double>* v = 0;

        std::string name = args[0];

        if (!ExpressionToken_Variable::IsValidVariableName(name)) {
            printf("Invalid character(s) in name: %s\n", name.c_str());
            return;
        }

        for (std::pair<std::string, Vector<double>*> p : mDefinedVectors) {
            if (p.first == name) {
                v = p.second;
                exists = true;
                break;
            }
        }

        if (!exists) {
            printf("There is nothing named %s\n", name.c_str());
            return;
        }

        double x = 0;
        char test;

        std::stringstream xStream(args[1]);

        if ((!(xStream >> x)) || (xStream >> test)) {
            printf("Invalid angle\n");
            return;
        }

        v->Rotate(x);
        renderer->RequestRedraw();
    } else if (cmd == "delete") {
        if (args.size() < 1) {
            printf("Missing argument(s)!\n");
            return;
        }

        std::map<std::string, Vector<double>*>::iterator it = mDefinedVectors.find(args[0]);

        if (it == mDefinedVectors.end()) {
            printf("There is nothing named '%s'!\n", args[0].c_str());
            return;
        }

        renderer->RemoveVector(it->second);
        mDefinedVectors.erase(it);
    } else if (cmd == "scale") {
        if (args.size() < 1) {
            printf("Missing argument(s)!\n");
            return;
        }

        double v;
        char test;

        std::stringstream vStream(args[0]);

        if ((!(vStream >> v)) || (vStream >> test)) {
            printf("Invalid sacle value\n");
            return;
        }

        renderer->SetRenderScale(v);
    } else if (cmd == "show" || cmd == "hide") {
        bool show = cmd == "show";

        if (args.size() < 1) {
            printf("Missing argument(s)!\n");
            return;
        }

        if (args[0] == "object") {
            if (args.size() < 2) {
                ListVisibilityObjects();
                return;
            }

            std::map<std::string, Vector<double>*>::iterator it = mDefinedVectors.find(args[1]);

            if (it == mDefinedVectors.end()) {
                printf("There is nothing named '%s'!\n", args[1].c_str());
                return;
            }

            it->second->SetVisible(show);
            renderer->RequestRedraw();
        } else if (args[0] == "feature") {

            if (args.size() < 2) {
                ListVisibilityFeatures(renderer);
                return;
            }

            if (args[1] == "basevector_x") {
                renderer->SetBaseVectorVisible('x', show);
            } else if (args[1] == "basevector_y") {
                renderer->SetBaseVectorVisible('y', show);
            } else if (args[1] == "basevector_z") {
                renderer->SetBaseVectorVisible('z', show);
            } else if (args[1] == "grid") {
                renderer->SetGridEnabled(show);
            } else if (args[1] == "span") {
                renderer->SetSpanVisible(show);
            } else {
                printf("Unknown feature: '%s'\n", args[1].c_str());
                return;
            }
        } else {
            printf("Invalid type: '%s'\n", args[0].c_str());
            return;
        }

    } else if (cmd == "expr") {
        if (args.size() < 1) {
            printf("Empty expression!\n");
            return;
        }

        std::string exprstr = "";

        for (std::string s : args) {
            exprstr += " " + s;
        }

        InterpretExpression(renderer, exprstr);
    } else {
        printf("Unknown command! Type 'help' for the list of commands!\n");
    }
}

void CommandLineProcessor::ListVisibilityObjects() {
    printf("-- Objects ---\n");

    for (std::pair<std::string, Vector<double>*> p : mDefinedVectors) {
        printf("  %-20s (%s)\n", p.first.c_str(), p.second->IsVisible() ? "visible" : "hidden");
    }
}

void CommandLineProcessor::ListVisibilityFeatures(Engine::RendererBase* renderer) {
    printf("-- Features ---\n");

    printf("  %-20s (%s)\n", "basevector_x", renderer->IsBaseVectorVisible('x') ? "visible" : "hidden");
    printf("  %-20s (%s)\n", "basevector_y", renderer->IsBaseVectorVisible('y') ? "visible" : "hidden");
    printf("  %-20s (%s)\n", "basevector_z", renderer->IsBaseVectorVisible('z') ? "visible" : "hidden");
    printf("  %-20s (%s)\n", "grid", renderer->IsGridEnabled() ? "visible" : "hidden");
    printf("  %-20s (%s)\n", "span", renderer->IsSpanVisible() ? "visible" : "hidden");
}

void CommandLineProcessor::ShowHelp() {
    printf("--- Available commands ---\n");
    printf(" help                        |  show help\n");
    printf(" exit                        |  exit the program\n");
    printf(" redraw                      |  force a full redraw\n");
    printf(" reset                       |  reset everything\n");
    printf(" delay t                     |  wait t milliseconds\n");
    printf(" vector name (x y) (z)       |  define/change/view a vector\n");
    printf(" delete name                 |  remove a vector\n");
    printf(" rotate name degrees         |  rotate a vector\n");
    printf(" scale value                 |  set render scale\n");
    printf(" show object/feature (name)  |  show something\n");
    printf(" hide object/feature (name)  |  hide something\n");
    printf(" oshift x y                  |  shift the starting point of all vectors\n");
    printf(" expr ...                    |  interpret an expression\n");
}

void CommandLineProcessor::InterpretExpression(Engine::RendererBase* renderer, const std::string& expr) {
    printf("Interpreting expression: %s\n", expr.c_str());

    ExpressionInterpreter* interpreter = new ExpressionInterpreter(&mDefinedVectors, renderer);

    interpreter->InterpretExpression(expr);
    renderer->RequestRedraw();

    delete interpreter;
}

}
