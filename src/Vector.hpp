
#ifndef _Vector_hpp_
#define _Vector_hpp_

#include <math.h>

namespace LVis {

template<typename T>
class Vector {
    public:
        Vector() : Vector(0, 0) { }

        Vector(T x, T y) :
            Vector(x, y, 0) { }

        Vector(T x, T y, T z) : visible(true) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        // Getter methods for x,y,z
        T GetX() { return x; }
        T GetY() { return y; }
        T GetZ() { return z; }

        // Setter methods for x,y,z
        void SetX(T x) { this->x = x; }
        void SetY(T y) { this->y = y; }
        void SetZ(T z) { this->y = z; }

        // Special setters for multiple components
        void SetXY(T x, T y) {
            this->x = x;
            this->y = y;
        }
        
        void SetXYZ(T x, T y, T z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        // Only 2D support
        void Rotate(T deg) {
            T newX, newY, rad;

            rad = (360 - deg) * PI / 180;

            newX = this->x * cos(rad) + this->y * sin(rad);
            newY = -this->x * sin(rad) + this->y * cos(rad);

            this->x = newX;
            this->y = newY;
        }

        // Visiblility setter & getter
        void SetVisible(bool v) { this->visible = v; }
        bool IsVisible() { return this->visible; }

        // --- Custom operators --- //

        // Equals & NotEquals
        bool Equals(const Vector<T>* other) {
            return (this->x == other->x && this->y == other->y && this->z == other->z);
        }

        bool operator==(const Vector<T>& other) {
            return this->Equals(&other);
        }

        bool operator!=(const Vector<T>& other) {
            return !this->Equals(&other);
        }


        // Multiplication by scalar
        Vector<T>& operator*=(const T& scalar) {
            this->SetXYZ(this->x * scalar, this->y * scalar, this->z * scalar);
            return *this;
        }

        friend Vector<T> operator*(const Vector<T>& left, const T& right) {
            return Vector<T>(left.x * right, left.y * right, left.z * right);
        }

        friend Vector<T> operator*(const T& left, const Vector<T>& right) {
            return Vector<T>(left * right.x, left * right.y, left * right.z);
        }


        Vector<T>& operator+=(const Vector<T>& other) {
            this->SetXYZ(this->x + other.x, this->y + other.y, this->z + other.z);
            return *this;
        }

        friend Vector<T> operator+(const Vector<T>& left, const Vector<T>& right) {
            return Vector<T>(left.x + right.x, left.y + right.y, left.z + right.z);
        }

        
        Vector<T>& operator-=(const Vector<T>& other) {
            this->SetXYZ(this->x - other.x, this->y - other.y, this->z - other.z);
            return *this;
        }

        friend Vector<T> operator-(const Vector<T>& left, const Vector<T>& right) {
            return Vector<T>(left.x - right.x, left.y - right.y, left.z - right.z);
        }

    private:
        T    x,
             y,
             z;
        bool visible;
};

}

#endif