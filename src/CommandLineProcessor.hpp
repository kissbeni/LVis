
#ifndef _CommandLineProcessor_hpp
#define _CommandLineProcessor_hpp

#include "Engine/RendererBase.hpp"

#include <string>
#include <sstream>
#include <vector>
#include <map>

namespace LVis {
    class CommandLineProcessor {
        public:
            static void ProcessCommand(Engine::RendererBase* renderer, const std::string& cmdstr);

        private:
            static void ProcessCommand(Engine::RendererBase* renderer, const std::string& cmd, const std::vector<std::string>& args);

            static void ListVisibilityObjects();

            static void ListVisibilityFeatures(Engine::RendererBase* renderer);

            static void ShowHelp();

            static void InterpretExpression(Engine::RendererBase* renderer, const std::string& expr);

            static std::map<std::string, Vector<double>*> mDefinedVectors;
    };
}

#endif
