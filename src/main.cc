
#include "Shared.hpp"
#include "Vector.hpp"
#include "CommandLineProcessor.hpp"
#include "Engine/OpenGLRenderer.hpp"

#include <iostream>
#include <cstdio>
#include <thread>
#include <string>

namespace LVis {
    int lvis_main(const int argc, const char* argv[]) {
        
        // Initialize the renderer
        Engine::OpenGLRenderer* renderer =
            new Engine::OpenGLRenderer(Rectangle2D(0, 0, 1024, 768), Engine::RendererMode::Render2D);

        // Create the window, and exit if fails
        if (!renderer->CreateWindow()) {
            printf("Failed to create window!\n");
            return 1;
        }

        renderer->SetRenderScale(20.0);
        renderer->SetBaseVectorVisible('x', true);
        renderer->SetBaseVectorVisible('y', true);
        renderer->SetBaseVectorVisible('z', false);
        
        std::thread([renderer]() -> void {
            srand(static_cast<unsigned int>(time(0)));

            mSleep(200);

            std::string input;

            while (!renderer->IsClosed()) {
                printf("LVis> ");

                if (std::getline(std::cin, input)) {
                    CommandLineProcessor::ProcessCommand(renderer, input);
                }

                mSleep(10);
            }
        }).detach();


        while (!renderer->IsClosed()) {
            renderer->Update();
        }

        return 0;
    }
}

int main(const int argc, const char* argv[]) {

    // Call the actual main function in the LVis namespace
    return LVis::lvis_main(argc, argv);
}
