
#include "OpenGLRenderer.hpp"

namespace LVis {
namespace Engine {

/// --- PUBLIC --- ///

void OpenGLRenderer::DrawLine(float startX, float startY, float endX, float endY, float width, ColorRGB color) {
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);

    glLineWidth(width);

    UseColorB(color.r, color.g, color.b);
    glOrtho(0, this->mWindowRect.w, 0, this->mWindowRect.h, 0, 20.);

    glBegin(GL_LINES);
    glVertex2f(startX, startY);
    glVertex2f(endX, endY);
    glEnd();

    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
}

void OpenGLRenderer::DrawText(std::string text, float x, float y, float scale, ColorRGB color) {

    // Activate corresponding render state  
    UseColorB(color.r, color.g, color.b);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(this->mVAO);

    // Iterate through all characters
    std::string::const_iterator c;
    for (c = text.begin(); c != text.end(); c++)  {
        GLFTCharacter ch = mFreeTypeCharacters[*c];

        GLfloat xpos = x + ch.Bearing.x * scale;
        GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

        GLfloat w = ch.Size.x * scale;
        GLfloat h = ch.Size.y * scale;

        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },            
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }           
        };

        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);

        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, this->mVBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Render quad
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
    }

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void OpenGLRenderer::UseColorB(unsigned char r, unsigned char g, unsigned char b) {
    UseColor(r / 255.0f, g / 255.0f, b / 255.0f);
}

void OpenGLRenderer::UseColor(float r, float g, float b) {
    glUseProgram(this->mFreeTypeShader);
    glUniform3f(glGetUniformLocation(this->mFreeTypeShader, "textColor"), r, g, b);
}

// --- PRIVATE --- ///

bool OpenGLRenderer::FinishCreateWindow() {
    glEnable(GL_DEPTH_TEST);

    glewExperimental = GL_TRUE;
    glewInit();

    // Define the viewport dimensions
    glViewport(0, 0, this->mWindowRect.w, this->mWindowRect.h);

    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (!LoadShaders()) {
        return false;
    }

    if (FT_Init_FreeType(&this->mFreeTypeLib)) {
        printf("[!] Could not init FreeType Library\n");
        return false;
    }

    if (FT_New_Face(this->mFreeTypeLib, "fonts/monofur.ttf", 0, &this->mFreeTypeFace)) {
        printf("[!] Failed to load font (monofur.ttf)\n");
        return false;
    }

    FT_Set_Pixel_Sizes(this->mFreeTypeFace, 0, this->mFontSize);

    if (!LoadFTGlyphs()) {
        return false;
    }

    glBindTexture(GL_TEXTURE_2D, 0);

    FT_Done_Face(this->mFreeTypeFace);
    FT_Done_FreeType(this->mFreeTypeLib);

    // Configure VAO/VBO for texture quads
    glGenVertexArrays(1, &this->mVAO);
    glGenBuffers(1, &this->mVBO);
    glBindVertexArray(this->mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->mVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    this->mInitialized = true;
    this->mFrameTime = 0;
    this->mLastFrameTime = 0;
    this->mLastRedraw = clock();

    printf("[*] Renderer initialized successfully!\n");

    return true;
}

bool OpenGLRenderer::LoadShaders() {
    std::string vertexCode;
    std::string fragmentCode;

    GLuint vertex, fragment;
    GLint success;

    if (!ReadTextFileToString("shaders/text.vs", vertexCode)) {
        printf("[!] Failed to read vertex shader!\n");
        return false;
    }

    if (!ReadTextFileToString("shaders/text.frag", fragmentCode)) {
        printf("[!] Failed to read fragment shader!\n");
        return false;
    }


    vertex = glCreateShader(GL_VERTEX_SHADER);
    const char* vertexCodeCS = vertexCode.c_str();

    glShaderSource(vertex, 1, &vertexCodeCS, NULL);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);

    if (!success) {
        printf("[!] Failed to compile vertex shader!\n");
        return false;
    }


    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    const char* fragmentCodeCS = fragmentCode.c_str();

    glShaderSource(fragment, 1, &fragmentCodeCS, NULL);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);

    if (!success) {
        printf("[!] Failed to compile fragment shader!\n");
        return false;
    }

    this->mFreeTypeShader = glCreateProgram();
    glAttachShader(this->mFreeTypeShader, vertex);
    glAttachShader(this->mFreeTypeShader, fragment);
    glLinkProgram(this->mFreeTypeShader);

    glGetProgramiv(this->mFreeTypeShader, GL_LINK_STATUS, &success);

    if (!success) {
        printf("[!] Failed to link shader program!\n");
        return false;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);

    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->mWindowRect.w), 0.0f, static_cast<GLfloat>(this->mWindowRect.h));
    glUseProgram(this->mFreeTypeShader);
    glUniformMatrix4fv(glGetUniformLocation(mFreeTypeShader, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    return true;
}

bool OpenGLRenderer::LoadFTGlyphs() {

    // Disable byte-alignment restriction
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    GLubyte c;
    for (c = 0; c < 128; c++) {

        // Load character glyph 
        if (FT_Load_Char(mFreeTypeFace, c, FT_LOAD_RENDER)) {
            printf("[!] Failed to load glyph for char %c!", c);
            return false;
        }

        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            this->mFreeTypeFace->glyph->bitmap.width,
            this->mFreeTypeFace->glyph->bitmap.rows,
            0,
            GL_RED,
            GL_UNSIGNED_BYTE,
            mFreeTypeFace->glyph->bitmap.buffer
        );

        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Now store character for later use
        GLFTCharacter character = {
            texture,
            glm::ivec2(this->mFreeTypeFace->glyph->bitmap.width, mFreeTypeFace->glyph->bitmap.rows),
            glm::ivec2(this->mFreeTypeFace->glyph->bitmap_left, mFreeTypeFace->glyph->bitmap_top),
            static_cast<GLuint>(this->mFreeTypeFace->glyph->advance.x)
        };

        this->mFreeTypeCharacters.insert(std::pair<GLchar, GLFTCharacter>(c, character));
    }

    printf("[*] Successfully loaded %d glyphs using FreeType!\n", c);

    return true;
}

void OpenGLRenderer::BeginDrawInternal() {
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->mWindowRect.w), 0.0f, static_cast<GLfloat>(this->mWindowRect.h));
    glUseProgram(this->mFreeTypeShader);
    glUniformMatrix4fv(glGetUniformLocation(mFreeTypeShader, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    glViewport(0, 0, this->mWindowRect.w, this->mWindowRect.h);

    glClearColor(0.33f, 0.33f, 0.33f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

}
}
