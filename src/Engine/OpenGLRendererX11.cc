
#ifndef WINDOWS

#include "OpenGLRenderer.hpp"

namespace LVis {
namespace Engine {

static GLint att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };

/// --- PUBLIC --- ///

bool OpenGLRenderer::CreateWindow() {
    this->mX11Display = XOpenDisplay(NULL);

    if (this->mX11Display == NULL) {
        printf("[!] Cannot connect to X server\n");
        return false;
    }

    this->mX11RootWindow = DefaultRootWindow(this->mX11Display);
    this->mX11VisualInfo = glXChooseVisual(this->mX11Display, 0, att);

    if (this->mX11VisualInfo == NULL) {
        printf("[!] No appropriate visual found\n");
        return false;
    } else {
        printf("[*] Selected visual: %p\n", (void *)this->mX11VisualInfo->visualid);
    }

    this->mX11ColorMap = XCreateColormap(
        this->mX11Display,
        this->mX11RootWindow,
        this->mX11VisualInfo->visual,
        AllocNone
    );

    this->mX11SetWindowAttr.colormap = this->mX11ColorMap;
    this->mX11SetWindowAttr.event_mask = ExposureMask;

    mX11Window = XCreateWindow(
        this->mX11Display,
        this->mX11RootWindow,
        this->mWindowRect.x,
        this->mWindowRect.y,
        this->mWindowRect.w,
        this->mWindowRect.h,
        0,
        this->mX11VisualInfo->depth,
        InputOutput,
        this->mX11VisualInfo->visual,
        CWColormap | CWEventMask,
        &this->mX11SetWindowAttr
    );

    XMapWindow(this->mX11Display, this->mX11Window);
    XStoreName(this->mX11Display, this->mX11Window, LVIS_WINDOW_NAME);

    Atom close = XInternAtom(this->mX11Display, "WM_DELETE_WINDOW", 0);
    XSetWMProtocols(this->mX11Display, this->mX11Window, &close, 1);

    this->mGLXContext = glXCreateContext(this->mX11Display, this->mX11VisualInfo, NULL, GL_TRUE);
    glXMakeCurrent(this->mX11Display, this->mX11Window, this->mGLXContext);

    int glx_major, glx_minor;
    glXQueryVersion(this->mX11Display, &glx_major, &glx_minor);
    printf("[*] GLX version: %d.%d\n", glx_major, glx_minor);

    return this->FinishCreateWindow();
}

void OpenGLRenderer::Update() {
    if (!this->mInitialized && !this->mWindowClosed) {
        return;
    }

    while (XPending(this->mX11Display)) {
        XNextEvent(this->mX11Display, &mX11Event);

        if (this->mX11Event.type == Expose) {
            XGetWindowAttributes(this->mX11Display, this->mX11Window, &this->mX11WindowAttr);
            this->mRedrawRequested = true;
            Draw();
        } else if (this->mX11Event.type == ClientMessage
            && this->mX11Event.xclient.data.l[0] == static_cast<long int>(XInternAtom(this->mX11Display, "WM_DELETE_WINDOW", true))) {
            glXMakeCurrent(this->mX11Display, None, NULL);
            glXDestroyContext(this->mX11Display, this->mGLXContext);
            XDestroyWindow(this->mX11Display, this->mX11Window);
            XCloseDisplay(this->mX11Display);
            this->mWindowClosed = true;
            return;
        }
    }

    if ((clock() - this->mLastRedraw) > (5 * CLOCKS_PER_SEC)) {
        this->mRedrawRequested = true;
    }

    Draw();

    if (this->mFrameTime && this->mFrameTime != this->mLastFrameTime) {
        this->mLastFrameTime = this->mFrameTime;

        char titleBuf[128];
        snprintf(titleBuf, 128, LVIS_WINDOW_NAME " (render time: %.2f ms)", this->mFrameTime * 1000);

        XStoreName(this->mX11Display, this->mX11Window, titleBuf);
    }
}

void OpenGLRenderer::Destroy() {
    if (!this->mInitialized) {
        return;
    }

    XEvent ev;
    memset(&ev, 0, sizeof(ev));

    ev.xclient.type = ClientMessage;
    ev.xclient.window = this->mX11Window;
    ev.xclient.message_type = XInternAtom(this->mX11Display, "WM_PROTOCOLS", true);
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = XInternAtom(this->mX11Display, "WM_DELETE_WINDOW", false);
    ev.xclient.data.l[1] = CurrentTime;
    XSendEvent(this->mX11Display, this->mX11Window, False, NoEventMask, &ev);

    while (!this->mWindowClosed) {
        mSleep(5);
    }
}

/// --- PROTECTED --- ///

void OpenGLRenderer::BeginDraw() {
    this->mWindowRect.w = this->mX11WindowAttr.width;
    this->mWindowRect.h = this->mX11WindowAttr.height;

    this->BeginDrawInternal();
}

void OpenGLRenderer::EndDraw() {
    glXSwapBuffers(this->mX11Display, this->mX11Window);

    this->mLastRedraw = clock();
}

}
}

#endif
