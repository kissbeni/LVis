
#ifndef _OpenGLRenderer_hpp_
#define _OpenGLRenderer_hpp_

#include "RendererBase.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <cmath>
#include <ctime>

#define GLEW_STATIC
#include <GL/glew.h>

#ifndef WINDOWS
#include <GLFW/glfw3.h>
#include <GL/glx.h>
#endif

#include <GL/glu.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <freetype/config/ftheader.h>
#include <freetype/freetype.h>

namespace LVis {
namespace Engine {
    
    struct GLFTCharacter {
        GLuint     TextureID;  // ID handle of the glyph texture
        glm::ivec2 Size;       // Size of glyph
        glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
        GLuint     Advance;    // Offset to advance to next glyph
    };

    class OpenGLRenderer : public RendererBase
    {
        public:
            OpenGLRenderer(Rectangle2D windowRect, RendererMode mode) :
                RendererBase(windowRect, mode) { }

            ~OpenGLRenderer() {
            }

            #ifdef WINDOWS
            LRESULT OpenGLRendererWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
            #endif

            bool CreateWindow() override;

            void Update() override;

            void Destroy() override;

            void DrawLine(float startX, float startY, float endX, float endY, float width, ColorRGB color) override;

            void DrawText(std::string text, float x, float y, float scale, ColorRGB color) override;

            void UseColorB(unsigned char r, unsigned char g, unsigned char b);

            void UseColor(float r, float g, float b);

        protected:
            void BeginDraw() override;

            void EndDraw() override;

        private:
            bool FinishCreateWindow();

            bool LoadShaders();

            bool LoadFTGlyphs();

            void BeginDrawInternal();

            FT_Library                      mFreeTypeLib;
            FT_Face                         mFreeTypeFace;
            GLuint                          mFreeTypeShader;
            std::map<GLchar, GLFTCharacter> mFreeTypeCharacters;
            GLuint                          mVAO,
                                            mVBO;

            #ifdef WINDOWS
            HDC                             mGLWHContext;
            HGLRC                           mGLContext;
            HWND                            mWindow;
            #else
            Display*                        mX11Display;
            Window                          mX11RootWindow;
            XVisualInfo*                    mX11VisualInfo;
            Colormap                        mX11ColorMap;
            XSetWindowAttributes            mX11SetWindowAttr;
            Window                          mX11Window;
            GLXContext                      mGLXContext;
            XWindowAttributes               mX11WindowAttr;
            XEvent                          mX11Event;
            #endif

            float                           mLastFrameTime;
            clock_t                         mLastRedraw;
    };

}
}

#endif
