
#ifndef _RenderBase_hpp_
#define _RenderBase_hpp_

#include "../Shared.hpp"
#include "../Vector.hpp"
#include <vector>

#ifdef DrawText
#undef DrawText
#endif

#ifdef CreateWindow
#undef CreateWindow
#endif

#ifdef WINDOWS
#define LVIS_WINDOW_CLASS "lviswindow"
#endif

#define LVIS_WINDOW_NAME "LVis window"

namespace LVis {
namespace Engine {

    enum class RendererMode {
        Render2D,
        // Render3D -> not supported yet
    };

    class RendererBase
    {
        public:
            RendererBase(Rectangle2D windowRect, RendererMode mode);

            ~RendererBase();

            // Creates the rendering window and set up everything for rendering
            // (overloaded by the actual renderer)
            virtual bool CreateWindow() {
                return false;
            }

            // Process window messages, render stuff, etc.
            virtual void Update() { }

            // Destroys the main window, renderer, etc.
            virtual void Destroy() { }

            // Draws a line from [startX, startY] to [endX, endY] with the specified color
            virtual void DrawLine(float startX, float startY, float endX, float endY, float width, ColorRGB color) { }

            // Draws scaled text centered at [x, y]
            virtual void DrawText(std::string text, float x, float y, float scale, ColorRGB color) { }

            // Draws a line from [startX, startY] to [endX, endY] as an arrow pointing to [endX, endY]
            void DrawArrow(float startX, float startY, float endX, float endY, float width, ColorRGB color, float headLen = 5);

            // Draw a vector
            void DrawVector(Vector<double>* vector, float width, ColorRGB color, bool transform = true, float baseX = 0, float baseY = 0);

            // Reset base vectors and clear added vectors
            void Reset();

            // Sets the scale to be used for rendering
            void SetRenderScale(double newScale);

            // Set a custom base vector for <axis> (x,y,z and X,Y,Z are accepted)
            void SetBaseVector(char axis, Vector<double>* v);

            // Set visibility of the base vector for <axis> (x,y,z and X,Y,Z are accepted)
            void SetBaseVectorVisible(char axis, bool visible);

            // Get visibility of the base vector for <axis> (x,y,z and X,Y,Z are accepted)
            bool IsBaseVectorVisible(char axis);

            // Adds a new vector to be rendered
            void AddVector(Vector<double>* newVector, bool force = false);

            // Virtually shift the origo point
            void SetVirtualOrigoShift(double ox, double oy);

            // Removes a vector (by pointer comparison)
            void RemoveVector(Vector<double>* vectorPtr);

            // Set the background grid enabled/disabled
            void SetGridEnabled(bool newVal);

            // Gets wheter the grid is visible
            bool IsGridEnabled();

            // Hide/Show the current base vectors' span
            void SetSpanVisible(bool v);

            // Gets wether the span is visible
            bool IsSpanVisible();

            // Request a redraw for the next update
            void RequestRedraw();

            // Gets if the window is closed (returns true only after CreateWindow() is called)
            bool IsClosed();

        protected:
            // Prepares for a fresh frame to be rendered
            virtual void BeginDraw() { }

            // Finishes the current frame
            virtual void EndDraw() { }

            void Draw();

            Rectangle2D                     mWindowRect;
            RendererMode                    mRenderMode;
            Vector<double>*                 mOriginalBaseVectors[3];
            Vector<double>*                 mBaseVectors[3];
            std::vector<Vector<double>*>    mVectors;
            bool                            mDrawGrid,
                                            mRedrawRequested,
                                            mInitialized,
                                            mRenderSpan,
                                            mWindowClosed;
            double                          mRenderScale,
                                            mVOSx, mVOSy;
            float                           mFrameTime;

            const int                       mFontSize = 48;

        private:
            // Draws the background grid for the coordinate system
            void DrawGrid();

            // Draws the base vectors
            void DrawBaseVectors();

            // Draw added vectors
            void DrawUserDefinedVectors();

            // Draw axes
            void DrawAxisLines();

            // Draw the current base vector's span
            void DrawSpanVectors();

            // Draw letters x,y,z for the axes
            void DrawAxisLetters();
    };

}
}

#endif
