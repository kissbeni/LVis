
#ifdef WINDOWS

#include "OpenGLRenderer.hpp"

namespace LVis {
namespace Engine {

static PIXELFORMATDESCRIPTOR att = {
    sizeof(PIXELFORMATDESCRIPTOR),
    1,
    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
    PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
    32,                   // Colordepth of the framebuffer.
    0, 0, 0, 0, 0, 0,
    0,
    0,
    0,
    0, 0, 0, 0,
    24,                   // Number of bits for the depthbuffer
    8,                    // Number of bits for the stencilbuffer
    0,                    // Number of Aux buffers in the framebuffer.
    PFD_MAIN_PLANE,
    0,
    0, 0, 0
};

LRESULT CALLBACK OpenGLRendererWndProcWrapper(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    return ((OpenGLRenderer*)GetWindowLongPtr(hWnd, GWLP_USERDATA))->OpenGLRendererWndProc(hWnd, message, wParam, lParam);
}

/// --- PUBLIC --- ///

LRESULT OpenGLRenderer::OpenGLRendererWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    
    // this CAN be null in this situation, because it's assigned after CreateWindowW is called
    if (this) {
        if (message == WM_PAINT || message == WM_SIZE) {
            this->mRedrawRequested = true;
        }

        if (message == WM_QUIT || message == WM_DESTROY) {
            wglDeleteContext(this->mGLContext);
            SetWindowLong(hWnd, GWLP_USERDATA, 0);
            this->mWindowClosed = true;
            PostQuitMessage(0);
            return 0;
        }
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

bool OpenGLRenderer::CreateWindow() {
    USES_CONVERSION;

    this->mWindowClosed = false;

    WNDCLASS wc = { 0 };
    wc.lpfnWndProc = OpenGLRendererWndProcWrapper;
    wc.hInstance = NULL;
    wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
    wc.lpszClassName = A2W(LVIS_WINDOW_CLASS);
    wc.style = CS_OWNDC;

    if (!RegisterClass(&wc))
        return false;

    this->mWindow = CreateWindowW(
        wc.lpszClassName,
        A2W(LVIS_WINDOW_NAME),
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        this->mWindowRect.x,
        this->mWindowRect.y,
        this->mWindowRect.w,
        this->mWindowRect.h,
        0,          // hWndParent
        0,          // hMenu
        NULL,       // hInstance
        NULL        // lParam
    );

    SetWindowLongPtr(this->mWindow, GWLP_USERDATA, (LONG_PTR)this);

    this->mGLWHContext = GetDC(this->mWindow);

    SetPixelFormat(this->mGLWHContext, ChoosePixelFormat(this->mGLWHContext, &att), &att);

    this->mGLContext = wglCreateContext(this->mGLWHContext);
    wglMakeCurrent(this->mGLWHContext, this->mGLContext);

    return this->FinishCreateWindow();
}

void OpenGLRenderer::Update() {
    USES_CONVERSION;

    if (!this->mInitialized && !this->mWindowClosed) {
        return;
    }

    MSG msg = { 0 };
    while (PeekMessage(&msg, this->mWindow, 0, 0, 1)) {
        DispatchMessage(&msg);
    }

    if ((clock() - this->mLastRedraw) > (5 * CLOCKS_PER_SEC)) {
        this->mRedrawRequested = true;
    }

    Draw();

    if (this->mFrameTime && this->mFrameTime != this->mLastFrameTime) {
        this->mLastFrameTime = this->mFrameTime;

        char titleBuf[128];
        snprintf(titleBuf, 128, LVIS_WINDOW_NAME " (render time: %.2f ms)", this->mFrameTime * 1000);

        SetWindowText(this->mWindow, A2W(titleBuf));
    }
}

void OpenGLRenderer::Destroy() {
    if (!this->mInitialized) {
        return;
    }

    PostMessage(this->mWindow, WM_QUIT, NULL, NULL);

    while (!this->mWindowClosed) {
        mSleep(5);
    }
}

/// --- PROTECTED --- ///

void OpenGLRenderer::BeginDraw() {
    RECT clientRect;
    GetClientRect(this->mWindow, &clientRect);
    this->mWindowRect.w = clientRect.right - clientRect.left;
    this->mWindowRect.h = clientRect.bottom - clientRect.top;

    this->BeginDrawInternal();
}

void OpenGLRenderer::EndDraw() {
    SwapBuffers(this->mGLWHContext);

    this->mLastRedraw = clock();
}

}
}

#endif
