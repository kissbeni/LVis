
#include "RendererBase.hpp"

#include <cstdio>
#include <cctype>
#include <ctime>

namespace LVis {
namespace Engine {

/// --- PUBLIC --- ///

RendererBase::RendererBase(Rectangle2D windowRect, RendererMode mode) {

    this->mWindowRect = windowRect;
    this->mRenderMode = mode;

    // Set original base vectors
    this->mOriginalBaseVectors[0] = new Vector<double>(1, 0, 0); // i
    this->mOriginalBaseVectors[1] = new Vector<double>(0, 1, 0); // j
    this->mOriginalBaseVectors[2] = new Vector<double>(0, 0, 1); // u

    // Set base vectors to the original base vectors
    this->mBaseVectors[0] = this->mOriginalBaseVectors[0]; // i
    this->mBaseVectors[1] = this->mOriginalBaseVectors[1]; // j
    this->mBaseVectors[2] = this->mOriginalBaseVectors[2]; // u

    // Do not draw background grid by default
    this->mDrawGrid = false;

    // Set the default scale to 1:1
    this->mRenderScale = 1.0;

    // This will set true by the renderer, when it is ready to draw stuff
    this->mInitialized = false;

    // Do not render span by default
    this->mRenderSpan = false;

    // Do not virtually shift origo
    this->mVOSx = 0;
    this->mVOSy = 0;

    // Initial render request
    this->RequestRedraw();
}

RendererBase::~RendererBase() {
    // Delete original base vectors
    delete this->mOriginalBaseVectors[0];
    delete this->mOriginalBaseVectors[1];
    delete this->mOriginalBaseVectors[2];
}

void RendererBase::DrawArrow(float startX, float startY, float endX, float endY, float width, ColorRGB color, float headLen /* = 5 */) {

    double vx = endX - startX;
    double vy = endY - startY;

    double nx = -vy;
    double ny = vx;

    double x1 = endX - nx - vx;
    double y1 = endY - ny - vy;

    double x2 = endX + nx - vx;
    double y2 = endY + ny - vy;

    double d = sqrt(pow(x1 - endX, 2) + pow(y1 - endY, 2));
    x1 = ((headLen * x1) + ((d - headLen) * endX)) / d;
    y1 = ((headLen * y1) + ((d - headLen) * endY)) / d;

    d = sqrt(pow(x2 - endX, 2) + pow(y2 - endY, 2));
    x2 = ((headLen * x2) + ((d - headLen) * endX)) / d;
    y2 = ((headLen * y2) + ((d - headLen) * endY)) / d;
    
    // Tail
    DrawLine(startX, startY, endX, endY, width, color);

    // Head
    DrawLine(endX, endY, x1, y1, width, color);
    DrawLine(endX, endY, x2, y2, width, color);
}

void RendererBase::DrawVector(Vector<double>* vector, float width, ColorRGB color,
            bool transform /* = true */, float baseX /* = 0 */, float baseY /* = 0 */) {

    float origoX = this->mWindowRect.w / 2;
    float origoY = this->mWindowRect.h / 2;

    if (vector->IsVisible()) {
        float   x = vector->GetX(),
                y = vector->GetY();

        if (transform) {
            x = this->mBaseVectors[0]->GetX() * vector->GetX() + 
                this->mBaseVectors[1]->GetX() * vector->GetX() + 
                this->mBaseVectors[2]->GetX() * vector->GetX();

            y = this->mBaseVectors[0]->GetY() * vector->GetY() + 
                this->mBaseVectors[1]->GetY() * vector->GetY() + 
                this->mBaseVectors[2]->GetY() * vector->GetY();
        }

        DrawArrow(
            origoX + baseX + this->mVOSx * this->mRenderScale,
            origoY + baseY + this->mVOSy * this->mRenderScale,
            origoX + x * this->mRenderScale + this->mVOSx * this->mRenderScale,
            origoY + y * this->mRenderScale + this->mVOSy * this->mRenderScale,
            width,
            color,
            8
        );
    }
}

void RendererBase::Reset() {

    // Restore base vectors
    for (int i = 0; i < 3; i++) {
        this->mBaseVectors[i] = this->mOriginalBaseVectors[i];
    }

    this->mVectors.clear();
    this->RequestRedraw();
}

void RendererBase::SetRenderScale(double newScale) {
    this->mRenderScale = newScale;
    this->RequestRedraw();
}

void RendererBase::SetBaseVector(char axis, Vector<double>* v) {

    // Find out the array index from the axis char
    int indx = 0;

    axis = tolower(axis);

    if (axis == 'y')
        indx = 1;
    else if (axis == 'z')
        indx = 2;
    else if (axis != 'x')
        throw "Invalid axis!";

    // Set new vector
    this->mBaseVectors[indx] = v;
    this->RequestRedraw();
}

void RendererBase::SetBaseVectorVisible(char axis, bool visible) {

    // Find out the array index from the axis char
    int indx = 0;

    axis = tolower(axis);

    if (axis == 'y')
        indx = 1;
    else if (axis == 'z')
        indx = 2;
    else if (axis != 'x')
        throw "Invalid axis!";

    // Set visibility
    this->mBaseVectors[indx]->SetVisible(visible);
    this->RequestRedraw();
}

bool RendererBase::IsBaseVectorVisible(char axis) {

    // Find out the array index from the axis char
    int indx = 0;

    axis = tolower(axis);

    if (axis == 'y')
        indx = 1;
    else if (axis == 'z')
        indx = 2;
    else if (axis != 'x')
        throw "Invalid axis!";

    return this->mBaseVectors[indx]->IsVisible();
}

void RendererBase::AddVector(Vector<double>* newVector, bool force /* = false */) {

    // Force will add the vector without any checks
    if (!force) {

        // Check if the vector is already added
        for (Vector<double>* v : this->mVectors) {
            if (v->Equals(newVector)) {
                return; // This vector is already exists
            }
        }
    }

    printf("[*] Adding new vector: [%.2f %.2f %.2f]\n", newVector->GetX(), newVector->GetY(), newVector->GetZ());
    this->mVectors.push_back(newVector);
    this->RequestRedraw();
}

void RendererBase::RemoveVector(Vector<double>* vectorPtr) {
    for (std::vector<Vector<double>*>::iterator it = this->mVectors.begin(); it != mVectors.end(); ++it) {
        if (((void*)vectorPtr) == ((void*)*it)) {
            this->mVectors.erase(it);
            this->RequestRedraw();
            return;
        }
    }
}

void RendererBase::SetVirtualOrigoShift(double ox, double oy) {
    this->mVOSx = ox;
    this->mVOSy = oy;
}

void RendererBase::SetGridEnabled(bool newVal) {
    this->mDrawGrid = newVal;
    this->RequestRedraw();
}

bool RendererBase::IsGridEnabled() {
    return this->mDrawGrid;
}

void RendererBase::SetSpanVisible(bool v) {
    this->mRenderSpan = v;
    this->RequestRedraw();
}

bool RendererBase::IsSpanVisible() {
    return this->mRenderSpan;
}

void RendererBase::RequestRedraw() {
    this->mRedrawRequested = true;
}

bool RendererBase::IsClosed() {
    return this->mInitialized && this->mWindowClosed;
}

/// --- PROTECTED --- ///

void RendererBase::Draw() {
    if (!this->mInitialized || !this->mRedrawRequested) {
        return;
    }

    this->mRedrawRequested = false;

    clock_t start = clock();

    this->BeginDraw();

    this->DrawBaseVectors();
    this->DrawUserDefinedVectors();
    this->DrawSpanVectors();

    this->DrawAxisLines();
    this->DrawAxisLetters();

    this->DrawGrid();

    this->EndDraw();

    this->mFrameTime = (float)(clock() - start) / (float)CLOCKS_PER_SEC;
}

/// --- PRIVATE --- ///

void RendererBase::DrawGrid() {
    if (!this->mDrawGrid) {
        return;
    }

    for (float x = this->mWindowRect.w / 2; x < this->mWindowRect.w - 15; x += this->mRenderScale * 2) {
        DrawLine(x, 0, x, this->mWindowRect.h, 0.5, ColorRGB(150, 150, 150));
    }

    for (float x = this->mWindowRect.w / 2; x > 15; x -= this->mRenderScale * 2) {
        DrawLine(x, 0, x, this->mWindowRect.h, 0.5, ColorRGB(150, 150, 150));
    }

    for (float y = this->mWindowRect.h / 2; y < this->mWindowRect.h - 20; y += this->mRenderScale * 2) {
        DrawLine(0, y, this->mWindowRect.w, y, 0.5, ColorRGB(150, 150, 150));
    }

    for (float y = this->mWindowRect.h / 2; y > 20; y -= this->mRenderScale * 2) {
        DrawLine(0, y, this->mWindowRect.w, y, 0.5, ColorRGB(150, 150, 150));
    }


    for (float x = this->mWindowRect.w / 2; x < this->mWindowRect.w - 15; x += this->mRenderScale) {
        DrawLine(x, 0, x, this->mWindowRect.h, 0.5, ColorRGB(66, 66, 66));
    }

    for (float x = this->mWindowRect.w / 2; x > 15; x -= this->mRenderScale) {
        DrawLine(x, 0, x, this->mWindowRect.h, 0.5, ColorRGB(66, 66, 66));
    }

    for (float y = this->mWindowRect.h / 2; y < this->mWindowRect.h - 20; y += this->mRenderScale) {
        DrawLine(0, y, this->mWindowRect.w, y, 0.5, ColorRGB(66, 66, 66));
    }

    for (float y = this->mWindowRect.h / 2; y > 20; y -= this->mRenderScale) {
        DrawLine(0, y, this->mWindowRect.w, y, 0.5, ColorRGB(66, 66, 66));
    }
}

void RendererBase::DrawBaseVectors() {
    for (int i = 0; i < 3; i++) {
        this->DrawVector(this->mBaseVectors[i], 2.5, ColorRGB(128, 188, 37), false);
    }
}

void RendererBase::DrawUserDefinedVectors() {
    for (Vector<double>* v : this->mVectors) {
        this->DrawVector(v, 2.5, ColorRGB(244, 194, 66));
    }
}

void RendererBase::DrawAxisLines() {

    this->DrawArrow(this->mWindowRect.w / 2, 0, this->mWindowRect.w / 2, this->mWindowRect.h, 2, ColorRGB(0, 0, 0), 10);
    this->DrawArrow(0, this->mWindowRect.h / 2, this->mWindowRect.w, this->mWindowRect.h / 2, 2, ColorRGB(0, 0, 0), 10);


    float startY = (this->mWindowRect.h / 2) + 5;
    float endY = (this->mWindowRect.h / 2) - 5;

    for (float x = this->mWindowRect.w / 2; x < this->mWindowRect.w - 15; x += this->mRenderScale) {
        this->DrawLine(x, startY, x, endY, 1.3, ColorRGB(0, 0, 0));
    }

    for (float x = this->mWindowRect.w / 2; x > 15; x -= this->mRenderScale) {
        this->DrawLine(x, startY, x, endY, 1.3, ColorRGB(0, 0, 0));
    }


    float startX = (this->mWindowRect.w / 2) + 5;
    float endX = (this->mWindowRect.w / 2) - 5;

    for (float y = this->mWindowRect.h / 2; y < this->mWindowRect.h - 20; y += this->mRenderScale) {
        this->DrawLine(startX, y, endX, y, 1.3, ColorRGB(0, 0, 0));
    }

    for (float y = this->mWindowRect.h / 2; y > 20; y -= this->mRenderScale) {
        this->DrawLine(startX, y, endX, y, 1.3, ColorRGB(0, 0, 0));
    }
}

void RendererBase::DrawSpanVectors() {
    if (!this->mRenderSpan) {
        return;
    }

    float unitsX = (this->mWindowRect.w / 2) / this->mRenderScale;
    float unitsY = (this->mWindowRect.h / 2) / this->mRenderScale;

    Vector<double> v;

    for (float x = -unitsX + 1; x < unitsX; x++) {
        for (float y = -unitsY + 1; y < unitsY; y++) {
            v.SetXY(x, y);
            this->DrawVector(&v, 1.8, ColorRGB(244, 66, 229));
        }
    }

}

void RendererBase::DrawAxisLetters() {
    DrawText("y", this->mWindowRect.w / 2 + 5, this->mWindowRect.h - (this->mFontSize / 2.0) * 0.5 - 5, 0.5, ColorRGB(255, 255, 255));
    DrawText("x", this->mWindowRect.w - (this->mFontSize / 2.0) * 0.5 - 5, this->mWindowRect.h / 2 + 5, 0.5f, ColorRGB(255, 255, 255));
}

}
}
