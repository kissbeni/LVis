
CXX = g++
CFLAGS  = -fPIC -pie -Wl,-E -g -Wall -O3 -lX11 -lGL -lGLU -lglfw `pkg-config --libs --static glew` -I./freetype/include -L./freetype/objs/.libs/ -lfreetype

all: lvis

clean:
	$(RM) *.o *~ lvis

lvis: RendererBase.o OpenGLRenderer.o OpenGLRendererX11.o ExpressionInterpreter.o CommandLineProcessor.o main.o
	$(CXX) $(CFLAGS) -o lvis RendererBase.o OpenGLRenderer.o OpenGLRendererX11.o ExpressionInterpreter.o CommandLineProcessor.o main.o

RendererBase.o: src/Engine/RendererBase.cc src/Engine/RendererBase.hpp src/Shared.hpp src/Vector.hpp
	$(CXX) $(CFLAGS) -c src/Engine/RendererBase.cc

OpenGLRenderer.o: src/Engine/OpenGLRenderer.cc src/Engine/OpenGLRenderer.hpp src/Shared.hpp src/Vector.hpp
	$(CXX) $(CFLAGS) -c src/Engine/OpenGLRenderer.cc

OpenGLRendererX11.o: src/Engine/OpenGLRendererX11.cc src/Engine/OpenGLRenderer.hpp src/Shared.hpp src/Vector.hpp
	$(CXX) $(CFLAGS) -c src/Engine/OpenGLRendererX11.cc

ExpressionInterpreter.o: src/ExpressionInterpreter.cc src/ExpressionInterpreter.hpp src/Shared.hpp src/Vector.hpp
	$(CXX) $(CFLAGS) -c src/ExpressionInterpreter.cc

CommandLineProcessor.o: src/CommandLineProcessor.cc src/CommandLineProcessor.hpp src/Shared.hpp src/Vector.hpp
	$(CXX) $(CFLAGS) -c src/CommandLineProcessor.cc

main.o: src/CommandLineProcessor.hpp src/Shared.hpp src/Vector.hpp src/main.cc
	$(CXX) $(CFLAGS) -c src/main.cc
